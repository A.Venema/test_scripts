I made a environment like this 
```
mamba create -n RODAM R==4.1.3 
```

I work on a node for Rstudio

```
salloc -w node012 -c 4 --mem 4G

ssh node012 -X

conda activate RODAM
module load R rstudio  
rstudio
```


at the bottom you see the packages I need : 
The packages that give trouble with the Rccp package are packages like:
```
BiocManager::install("MethylAid")
BiocManager::install("bumphunter")
```


I tried the normal install and:
```
install.packages("Rcpp", repos="https://rcppcore.github.io/drat", dependencies = TRUE)


install.packages("reshape")

install.packages("data.table")
install.packages("dplyr")
install.packages("expss")
install.packages("fields")
install.packages("ggplot2")
install.packages("pheatmap")
install.packages("plyr")
install.packages("preprocessCore")
install.packages("RColorBrewer")
install.packages("splitstackshape")
install.packages("svd")
install.packages("vegan")
install.packages("viridis")



if (!require("BiocManager", quietly = TRUE))
  install.packages("BiocManager")

BiocManager::install("bumphunter")
BiocManager::install("bacon")
BiocManager::install("affy")
BiocManager::install("ChAMP")
BiocManager::install("IlluminaHumanMethylation450kmanifest")
BiocManager::install("IlluminaHumanMethylationEPICanno.ilm10b4.hg19")
BiocManager::install("IlluminaHumanMethylationEPICmanifest")
BiocManager::install("limma")
BiocManager::install("lumi")
BiocManager::install("MethylAid")
BiocManager::install("minfi")
BiocManager::install("minfiData")
BiocManager::install("missMethyl")

BiocManager::install("DMRcate")
BiocManager::install("vsn")
#BiocManager::install("BiodiversityR") # werkt niet, mogelijk niet nodig
```
